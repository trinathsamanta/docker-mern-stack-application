### Build docker image
```sh
docker-compose build
```

### Run docker container using existing image
```sh
docker-compose up -d
```

### List all ruuning docker processes
```sh
docker ps
```

### Log & debug docker processes
```sh
docker logs -f processId
```

### Hurray!! app is now ready for development
For frontend application: [http://localhost:3000](http://localhost:3000)

For backend application: [http://localhost:4000/api/fruits](http://localhost:4000/api/fruits)
