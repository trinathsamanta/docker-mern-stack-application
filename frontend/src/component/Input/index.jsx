import React from 'react';
import './styles.scss';

export const Input = ({
  title = 'input-text',
  name = 'input-text',
  placeholder = 'Type text input',
  ...props
}) => {

  return (
    <input title={title} name={name} type="text" placeholder={placeholder} className={'input-text font-color-light pad-md font-md full-width'} {...props}></input>
  );
};

