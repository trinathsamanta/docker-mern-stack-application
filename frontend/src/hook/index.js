import { useState, useEffect } from 'react';

export const intialState = {
  isReady: false,
  isLoading: false,
  data: undefined,
  error: undefined
};

export const baseUrl = 'http://localhost:4000/api';

export const useFruits = (data) => {
  const [state, setState] = useState({ ...intialState, data, isLoading: true });

  useEffect(() => {
    fetch(`${baseUrl}/fruits`).then(r => {
      return r.json();
    }).then(r => {
      setState({ ...state, data: r.data, isReady: true, isLoading: false });
    }).catch(e => {
      setState({ ...state, error: e, isReady: true, isLoading: false });
    });
  }, []);

  return [{ ...state }];
};
